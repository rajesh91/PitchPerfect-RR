//
//  RecordSoundsViewController.swift
//  PitchPerfect
//
//  Created by Rajesh Ramachandiran on 10/7/15.
//  Copyright (c) 2015 Rajesh Ramachandiran. All rights reserved.
//

import UIKit
import AVFoundation

class RecordSoundsViewController: UIViewController, AVAudioRecorderDelegate {
    
    @IBOutlet weak var recordingLabel: UILabel!
    @IBOutlet weak var stopButtonOutlet: UIButton!
    @IBOutlet weak var recordButtonOutlet: UIButton!

    var audioRecorder: AVAudioRecorder!
    var recordedAudio: RecordedAudio!
    
    override func viewWillAppear(animated: Bool) {
        recordButtonOutlet.enabled = true
        recordingLabel.text = "Tap to record"
        recordingLabel.hidden = false
        println("Tap to begin recording")
    }

    @IBAction func recordButton(sender: UIButton) {

        recordingLabel.text = "Recording in progress..."
        stopButtonOutlet.hidden = false
        recordButtonOutlet.enabled = false
        
        let dirPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) [0] as!String
        
        let recordingName = "my_audio.wav"
        let pathArray = [dirPath,recordingName]
        
        let filePath = NSURL.fileURLWithPathComponents(pathArray)
        println(filePath)
        
        var session = AVAudioSession.sharedInstance()
        session.setCategory(AVAudioSessionCategoryPlayAndRecord, error: nil)
        
        audioRecorder = AVAudioRecorder(URL: filePath, settings: nil, error: nil)
        audioRecorder.delegate = self
        audioRecorder.meteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
        
    }
    
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder!, successfully flag: Bool) {
        if (flag) {

            recordedAudio = RecordedAudio(forPath: recorder.url, forTitle: recorder.url.lastPathComponent)

            self.performSegueWithIdentifier("stopRecording", sender: recordedAudio)
            println("Segue-Sucessfully passed audio file to effects screen. File at:")
            println( recordedAudio.filePathURL )
        }
        
        else {
            println("Recording failed!")
            recordButtonOutlet.enabled = true
            stopButtonOutlet.hidden = true
        }
        
    }
    
    @IBAction func stopButtonAction(sender: UIButton) {
        stopButtonOutlet.hidden = true
        recordingLabel.hidden = true
        audioRecorder.stop()
        var audioSession = AVAudioSession.sharedInstance()
        audioSession.setActive(false, error: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "stopRecording") {
            let playSoundsVC: PlaySoundsViewController = segue.destinationViewController as! PlaySoundsViewController
            let data = sender as! RecordedAudio
            playSoundsVC.recievedAudio = data
        }
        
    }
    

}

