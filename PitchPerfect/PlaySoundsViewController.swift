//
//  PlaySoundsViewController.swift
//  PitchPerfect
//
//  Created by Rajesh Ramachandiran on 10/7/15.
//  Copyright (c) 2015 Rajesh Ramachandiran. All rights reserved.
//

import UIKit
import AVFoundation

class PlaySoundsViewController: UIViewController {

    var audioPlayer:AVAudioPlayer!          // Declare global variables
    var recievedAudio: RecordedAudio!
    var audioEngine: AVAudioEngine!
    var audioFile: AVAudioFile!
    var audioSession: AVAudioSession = AVAudioSession.sharedInstance()
    
    @IBOutlet weak var currentSpeedText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        audioEngine = AVAudioEngine()       // Initialise audioEngine
        audioPlayer = AVAudioPlayer(contentsOfURL: recievedAudio.filePathURL, error: nil)
        audioPlayer.enableRate = true
        
        audioFile = AVAudioFile(forReading: recievedAudio.filePathURL, error: nil)      // Initialise audioFile
        audioSession.setCategory(AVAudioSessionCategoryPlayback, withOptions: AVAudioSessionCategoryOptions.DefaultToSpeaker, error: nil)

        currentSpeedText.hidden = true
        
    }

    @IBAction func playSlowSound(sender: UIButton) {
        playAudioWithVariablePitchAndRate(forPitch: 0, atRate: 0.5)
    }
    
    @IBAction func playFastSound(sender: UIButton) {
        playAudioWithVariablePitchAndRate(forPitch: 0, atRate: 2.1)
    }
    
    @IBAction func playChipmunk(sender: UIButton) {
        playAudioWithVariablePitchAndRate(forPitch: 1000, atRate: 1.05)
    }
    
    @IBAction func playDarthVader(sender: UIButton) {
        playAudioWithVariablePitchAndRate(forPitch: -800, atRate: 0.9)
    }
    
   @IBAction func stopSound(sender: UIButton) {
        stopAudio()
        currentSpeedText.text = ""
    } 
    
    func playAudioWithVariablePitchAndRate (forPitch pitch: Float, atRate rate: Float) {
        
        stopAudio()
        
        var audioPlayerNode = AVAudioPlayerNode()   // Create an instance of audio player node
        audioEngine.attachNode(audioPlayerNode)
        
        var soundEffectNode = AVAudioUnitTimePitch() // Change pitch and rate to the input parameters
        soundEffectNode.pitch = pitch
        soundEffectNode.rate = rate
        
        audioEngine.attachNode(soundEffectNode)
        
        audioEngine.connect(audioPlayerNode, to: soundEffectNode, format: nil) // Connect all the nodes sequentially
        audioEngine.connect(soundEffectNode, to: audioEngine.outputNode, format: nil)
        audioPlayerNode.scheduleFile(audioFile, atTime: nil, completionHandler: nil)
        audioEngine.startAndReturnError(nil)
        
        audioPlayerNode.play()
        
    }
    
    func stopAudio() {
        
        audioPlayer.stop()  // Stop all audio
        audioEngine.stop()
        audioEngine.reset()
        
    }

}
