//
//  RecordedAudio.swift
//  PitchPerfect
//
//  Created by Rajesh Ramachandiran on 18/7/15.
//  Copyright (c) 2015 Rajesh Ramachandiran. All rights reserved.
//

import Foundation

class RecordedAudio: NSObject {
    
    var filePathURL: NSURL!
    var title: String!
    
    init(forPath filePathURL: NSURL!, forTitle title: String!) {
        self.filePathURL = filePathURL
        self.title = title
    }
    
}
